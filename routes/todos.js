var express = require('express');
var bodyParser = require("body-parser");
var router = express.Router();
var jsonParser = bodyParser.json();
var mongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/todos";

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var todoSchema = new Schema({
    text:  String,
    complete: Boolean,
    id:   Number
},
{
    versionKey: false
});

mongoose.connect('mongodb://localhost:27017/todos');
var Todo = mongoose.model("Todo", todoSchema);

router.get('/', function(req, res){
    if(!req.query) return res.status(400).send({message: 'Page and limit are required.'});

    var todosPerPage = +req.query.limit;
    var hasComplete = (req.query.complete === 'undefined')? {} : {complete: req.query.complete};
    var query = Todo.find(hasComplete, null, { skip: (req.query.page - 1)* todosPerPage, limit: todosPerPage });

    Todo.find(hasComplete).count(function(err, count){
        if(err) return res.status(400).send({message: 'Page, limit or complete request error.'});
        var numPages = Math.ceil(count/todosPerPage);

        query.exec(function (err, todos) {

            if(err) return res.status(400).send({message: 'Todos request error.'});
            res.send({todos: todos, numPages: numPages, count: count});
        });
    });

});

router.get('/active/count', function(req, res){

    Todo.find({ complete: false }).count(function(err, countCompleted){
        if(err) return res.status(400).send({message: 'Active todo count request error.'});
        res.send(`${countCompleted}`);
    });

});

router.get('/completed/count', function(req, res){

    Todo.find({ complete: true }).count(function(err, countActive){
        if(err) return res.status(400).send({message: 'Completed todo count request error.'});
        res.send(`${countActive}`);
    });

});

router.post("/", jsonParser, function (req, res) {
    if(!req.body) return res.status(400).send({message: 'Text and complete are required'});

    Todo.create({
        text: req.body.text,
        complete: req.body.complete
    }, function (err, todo) {
        if(err) return res.status(400).send({message: 'Added todo request error.'});
        res.json(todo);
    });
});

router.put("/id/:id", jsonParser, function(req, res){

    if(!req.body) return res.status(400).send({message: 'Text and complete are required'});

    Todo.findByIdAndUpdate({ _id: req.params.id }, { text: req.body.text, complete: req.body.complete }, function (err, todo) {
        if(err) return res.status(400).send({message: 'Text and complete request error.'});
        res.send(todo);
    });

});

router.put("/complete", jsonParser, function(req, res){

    if(!req.body) return res.status(400).send({message: 'Complete true is required'});

    Todo.update({ complete: false }, { complete: true }, {multi: true}, function (err, todo) {
        if(err) return res.status(400).send({message: 'Complete request error.'});
        res.send(todo);
    });

});

router.delete("/id/:id", function(req, res){

        Todo.remove({
            _id: req.params.id
        }, function (err, todo) {
            if(err) return res.status(400).send({message: 'Delete current todo request error.'});
            res.send(todo);
        });
});

router.delete("/complete", function(req, res){

    Todo.remove({
        complete: true
    }, function (err, todo) {
        if(err) return res.status(400).send({message: 'Delete completed todo request error.'});
        res.send(todo);
    });
});

module.exports = router;
