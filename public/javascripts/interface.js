'use strict';

var todos = [], newText, thisPage = 1, numPages;

$(document).ready(function () {
    showTodosFromBd();

    // Add new line
    $("form").submit(function (e) {
        e.preventDefault();
        var valueInput = $.trim($(".new-text").val());
        if(valueInput.length) {
            addNewTodo();
            showTodosFromBd();
        }
    });

    // Completed this line

    $(".app").on('click', 'input:checkbox', function() {
        var checked = $(this).is(':checked');
        var defaultText = $(this).siblings('.text').html();
        var indexParentElement = $(this).parent().attr('data-id');

        changeTodo(indexParentElement, defaultText, checked);
    });

    // Completed all lines

    $('#all-completed-btn').on('click', completeAllTodo);

    // Deleted all completed lines

    $('.app').on('click', '#delete-completed-btn', DeleteAllCompletedTodo);

    // Deleted current line

    $('.app').on('click', '.delete-btn', function() {

        var indexElementForDel = $(this).parent().attr('data-id');
        DeleteCurrentTodo(indexElementForDel);
    });

    // Activate edit mode for line

    $(".app").on('dblclick', '.text', todoEdit);

    // Control visible lines

    $('.btn-filter').on('click', function () {
        $('.btn-filter').removeClass('active');
        $(this).addClass('active');
        thisPage = 1;
        showTodosFromBd();
    });

    $('.wrap-pagination').on('click', 'li', function() {
        thisPage = $(this).index() + 1;
        showTodosFromBd();
    });

});

// functions

// Add new line
function addNewTodo() {
    newText = $('.new-text').val().trim();
    $('.new-text').val('');
    createTodo();
}

// Check line on changing of text or completing and updating it in db
function changeTodo(idTodo, textTodo, completeTodo) {
    $.ajax({
        method: "PUT",
        url: "/todos/id/" + idTodo,
        data: JSON.stringify({
            text: textTodo,
            complete: completeTodo
        }),
        contentType: "application/json",
        success: function (data) {
            showTodosFromBd();
        }
    });
}

// Completed all todos in db
function completeAllTodo() {
    $.ajax({
        method: "PUT",
        url: "/todos/complete",
        contentType: "application/json",
        data: JSON.stringify({
            complete: true
        }),
        success: function (data) {
            showTodosFromBd();
        }
    });
}

// Add new line in db
function createTodo() {
    $.ajax({
        type: "POST",
        url: "/todos",
        data: JSON.stringify({text: newText, complete: false}),
        dataType: "json",
        contentType: "application/json",
        success: function(data){
            todos.push(data);
        }
    });
}

// Delete all completed todos in db
function DeleteAllCompletedTodo() {
    $.ajax({
        url: "/todos/complete",
        contentType: "application/json",
        method: "DELETE",
        success: function (data) {
            showTodosFromBd();
        }
    });
}

// Delete current line in db
function DeleteCurrentTodo(idTodo) {
    $.ajax({
        url: "/todos/id/" + idTodo,
        contentType: "application/json",
        method: "DELETE",
        success: function (idTodo) {
            showTodosFromBd();
        }
    });
}

// Show all todos from db
function getTodos(page, limit, complete) {
    $.ajax({
        url: "todos?page=" + page + "&complete=" + complete + "&limit=" + limit,
        type: "GET",
        contentType: "application/json",
        success: function (returnedTodos) {
            todos = returnedTodos.todos;
            numPages = returnedTodos.numPages;
            pagination(numPages, todos);
            render(todos);
            if (!todos.length && returnedTodos.count) {
                showTodosFromBd();
            }
        }
    });
}

// Pagination
function pagination(numPages, tasksOnPage) {
    numPages = numPages;
    var paginationStart = '<ul id="pagination">';
    var paginationEnd = '</ul>';
    var paginationTemplate = '<li><a href="javascript:void(0);">';
    var paginationTemplateEnd = '</a></li>';
    var paginationResult = '';

    if (numPages == 0) {
        $(".wrap-pagination").html('');
    }

    for (var i = 1; i <= numPages; i++) {

        if (i != thisPage) {
            paginationResult += paginationTemplate  + i + paginationTemplateEnd;
            $(".wrap-pagination").html(paginationStart + paginationResult + paginationEnd);
        } else {
            paginationResult += '<li class="active">' + i + '</li>';
            $(".wrap-pagination").html(paginationStart + paginationResult + paginationEnd);
        }

        if (!tasksOnPage.length) {
            thisPage = numPages;
        }
    }
}

// render list of tasks
function render(tasksFiltered) {

    $( ".tasks" ).html('');
    tasksFiltered.forEach(function(item, index, arr){

        $( ".tasks" ).append( "<div class='line" + (item.complete? ' completed': '') + (item.text == 'The string must not be empty!'? ' error': '') + "' data-id='" + item._id + "'><input type='checkbox'><span class='text'>" + item.text + "</span><button class='delete-btn'>X</button></div>");

        if (item.complete) {
            $('.line').eq(index).find('input:checkbox').prop('checked', true);
        }

    });

}

function showTodosFromBd() {
    var activeBtn = $('.btn-filter.active').attr('data-btn');

    switch (activeBtn) {
        case 'show-all-btn':
            getTodos(thisPage, 4);
            break;
        case 'show-active-btn':
            getTodos(thisPage, 4, false);
            break;
        case 'show-completed-btn':
            getTodos(thisPage, 4, true);
    }
    todosActiveCount();
    todosCompletedCount();
}

// Edit mode for line
function todoEdit() {
    var spanHtml = $(this).html();
    var editableText = $("<input type='text' class='edit-text'/>");
    editableText.val(spanHtml);
    $(this).replaceWith(editableText);
    // editableText.focus();
    editableText.focus(function () {
        $(this).on('keypress', function(e) {
            if(e.which == 13) {
                todoEditExit.call(this);
            }
        });

    });
    // setup the blur event for this new input
    editableText.blur(todoEditExit); // exit edit mode with blur

}

// Exit edit mode for line
function todoEditExit() {
    var indexElementForEdit = $(this).closest('.line').attr('data-id');
    var editableHtml = ($(this).val())? $(this).val() : 'The string must not be empty!';
    var viewableText = $("<span class='text'>");
    var checkedDefault = $(this).closest('.line').find('input').is(':checked');
    viewableText.html(editableHtml);
    $(this).replaceWith(viewableText);

    changeTodo(indexElementForEdit, editableHtml, checkedDefault);
}

// Counter of active todos
function todosActiveCount() {
    $.ajax({
        url: "todos/active/count",
        type: "GET",
        contentType: "application/json",
        success: function (countActive) {
            $('.active-counter').html(countActive);
        }
    });
}

// Counter of completed todos
function todosCompletedCount() {
    $.ajax({
        url: "todos/completed/count",
        type: "GET",
        contentType: "application/json",
        success: function (countCompleted) {
            $('.completed-counter').html(countCompleted);
        }
    });
}